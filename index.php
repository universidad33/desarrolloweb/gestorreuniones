<?php include 'model/Connection.php' ?>
<?php include 'template/header.php' ?>
<!-- Button trigger modal -->
<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal" style="float: right; margin-right: 20px; margin-top: 20px;">
    Agregar profesor
</button>

<div class="container">
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">Nombre Completo</th>
                <th scope="col">Identificación</th>
                <th scope="col">Celular</th>
                <th scope="col">Email</th>
                <th scope="col">Opciones</th>
            </tr>
        </thead>
        <tbody>
            <?php include 'getAllProfesor.php' ?>
        </tbody>
    </table>
</div>


<!-- Modal para Agregar profesor -->

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Agregar profesor</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <?php include './template/nuevoProfesor.php' ?>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="ModalProfesor" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar profesor</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <?php include './template/nuevoFuncionario.php' ?>
            </div>
        </div>
    </div>
</div>

<?php include 'template/footer.php' ?>