<form action="./newProfesor.php" method="POST">
  <div class="row">
    <div class="col">
      <input type="text" class="form-control" placeholder="Nombre" name="nombre" required>
    </div>
    <div class="col">
      <input type="number" class="form-control" placeholder="Identificacion" name="identificacion" required>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col">
      <input type="email" class="form-control" placeholder="Email" name="email" required>
    </div>
    <div class="col">
      <input type="number" class="form-control" placeholder="Celular" name="celular" required>
    </div>
  </div>

  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-primary">
  </div>
</form>